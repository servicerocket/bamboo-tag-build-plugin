package net.customware.bamboo.plugin.tagbuild.finder;

import com.atlassian.bamboo.author.ExtendedAuthor;
import com.atlassian.bamboo.author.ExtendedAuthorManager;
import com.atlassian.bamboo.build.fileserver.BuildDirectoryManager;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.repository.svn.SVNPathRevision;
import com.atlassian.bamboo.repository.svn.SvnRepository;
import com.atlassian.bamboo.resultsummary.BuildResultsSummaryManager;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import net.customware.bamboo.plugin.tagbuild.bean.Tag;
import net.customware.bamboo.plugin.tagbuild.exception.TagFinderException;
import org.apache.log4j.Logger;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNAuthenticationManager;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import java.io.File;
import java.util.*;

public class SvnTagFinder extends AbstractTagFinder {

    private static final Logger log = Logger.getLogger(SvnTagFinder.class);

    private static final String AUTH_SSH = "ssh";

    private static final String REAL_SVN_REPOSITORY_URL = "custom.tagbuild.svn.realRepositoryUrl";

    private static final String REAL_SVN_LATEST_REVISION = "custom.tagbuild.svn.realLatestRevision";

    private File workingDirectory;

    private BuildResultsSummaryManager buildResultsSummaryManager;

    private ExtendedAuthorManager extendedAuthorManager;

    private BuildDirectoryManager buildDirectoryManager;

    public void setBuildDirectoryManager(BuildDirectoryManager buildDirectoryManager) {
        this.buildDirectoryManager = buildDirectoryManager;
    }

    public void setBuildResultsSummaryManager(BuildResultsSummaryManager buildResultsSummaryManager) {
        this.buildResultsSummaryManager = buildResultsSummaryManager;
    }

    public void setExtendedAuthorManager(ExtendedAuthorManager extendedAuthorManager) {
        this.extendedAuthorManager = extendedAuthorManager;
    }

    @SuppressWarnings("unchecked")
    public Collection<Tag> findTags(Plan plan, BuildConfiguration buildConfiguration) throws TagFinderException {
        try {
            SVNRepository svnRepo = getSVNRepository(plan);

            if (svnRepo != null) {

                List<SVNDirEntry> tagEntries = new ArrayList<SVNDirEntry>();
                tagEntries.addAll(svnRepo.getDir("", -1, null, (Collection) null));
                Collections.sort(tagEntries, Collections.reverseOrder());
                List<Tag> tags = new ArrayList<Tag>();

                for (SVNDirEntry tagDir : tagEntries) {
                    tags.add(new Tag(buildResultsSummaryManager, plan, tagDir.getName(), tagDir.getDate(), getAuthor(tagDir.getAuthor())));
                }

                return tags;
            }
            return null;
        } catch (SVNException e) {
            log.error(e, e);
            throw new TagFinderException(e.getMessage(), e);
        }
    }

    private ExtendedAuthor getAuthor(String authorName) {
        if (authorName != null) {
            ExtendedAuthor author = extendedAuthorManager.getExtendedAuthorByName(authorName);
            log.debug("author = " + author);
            return author;
        }
        return null;
    }

    @SuppressWarnings("unused")
    private SVNRepository getSVNRepository(Plan plan) throws SVNException {

        SvnRepository repository = (SvnRepository) plan.getBuildDefinition().getRepository();
        String url = getTagsUrl(plan, null);

        SVNURL svnurl = SVNURL.parseURIEncoded(url);

        List<SVNPathRevision> externalsUpdated = new ArrayList<SVNPathRevision>();
        File sourceCodeDirectory = getSourceCodeDirectory(plan);

        SVNRepository svnRepo = null;
        if (AUTH_SSH.equals(repository.getAuthType())) {
            svnRepo = getSSHRepository(svnurl, repository.getUsername(), repository.getKeyFile(), repository.getPassphrase());
        } else {
            svnRepo = getRepository(svnurl, repository.getUsername(), repository.getUserPassword());
        }

        return svnRepo;
    }

    private String getTagsUrl(Plan plan, String tag) {
        SvnRepository repository = (SvnRepository) plan.getBuildDefinition().getRepository();
        Map<String, String> configs = plan.getBuildDefinition().getCustomConfiguration();

        String url = null;

        if (configs.containsKey(REAL_SVN_REPOSITORY_URL)) {
            url = configs.get(REAL_SVN_REPOSITORY_URL);
        } else {
            url = repository.getRepositoryUrl();
        }

        url = url.replaceAll("/trunk", "/tags");

        return url;
    }

    /**
     * For Subversion, we use the build name's directory within the system's
     * working directory
     *
     * @param plan
     * @return The source code directory
     */
    public File getSourceCodeDirectory(Plan plan) {

        File sourceCodeDirectory = null;
        try {
            sourceCodeDirectory = plan.getSourceCodeDirectory();
            sourceCodeDirectory.mkdirs();
        } catch (RepositoryException e) {
            log.error("Error getting source directory", e);
        }

        return sourceCodeDirectory;
    }

    public File getWorkingDirectory2() {
        if (workingDirectory == null) {
            // workingDirectory = BambooContainer.getBambooContainer().getBuildDirectory();
            // replace the above with below (getBambooContainer().getBuildDirectory() was removed in 3.x)
            workingDirectory = buildDirectoryManager.getWorkingDirectoryOfCurrentAgent();
        }

        return workingDirectory;
    }

    private SVNRepository getRepository(SVNURL svnurl, String username, String userPassword) throws SVNException {
        // BAM-1085, BAM-890, BAM-1028 - don't try to create a new {@link
        // SVNRepositoryImpl} object for getting the latest revision number.
        // Use the interface provided by SVNKit.
        DefaultSVNAuthenticationManager svnAuthenticationManager = new DefaultSVNAuthenticationManager(null,
                false, username, userPassword);
        return getSvnClientManager(svnAuthenticationManager).createRepository(svnurl, false);
    }

    private SVNRepository getSSHRepository(SVNURL svnurl, String username, String privateKeyFile,
                                           String passphrase) throws SVNException {
        // BAM-1085, BAM-890, BAM-1028 - don't try to create a new {@link
        // SVNRepositoryImpl} object for getting the latest revision number.
        // Use the interface provided by SVNKit.
        DefaultSVNAuthenticationManager svnAuthenticationManager = new DefaultSVNAuthenticationManager(null,
                false, username, null, new File(privateKeyFile), passphrase);
        return getSvnClientManager(svnAuthenticationManager).createRepository(svnurl, false);
    }

    private SVNClientManager getSvnClientManager(ISVNAuthenticationManager authenticationManager) {
        return SVNClientManager.newInstance(SVNWCUtil.createDefaultOptions(true), authenticationManager);
    }

    private SVNClientManager getSvnClientManager(SvnRepository repository) {
        if (AUTH_SSH.equals(repository.getAuthType())) {
            return SVNClientManager.newInstance(SVNWCUtil.createDefaultOptions(true),
                    new DefaultSVNAuthenticationManager(null, false, repository.getUsername(), null, new File(
                            repository.getKeyFile()), repository.getPassphrase()));
        } else {
            return SVNClientManager.newInstance(SVNWCUtil.createDefaultOptions(true),
                    new DefaultSVNAuthenticationManager(null, false, repository.getUsername(), repository
                            .getUserPassword()));
        }
    }

    public void tagBuildConfig(Plan plan, String tag, Repository repo, Map<String, String> customConfig) {
        if (repo instanceof SvnRepository) {
            SvnRepository svnRepo = (SvnRepository) repo;
            customConfig.put(REAL_SVN_REPOSITORY_URL, svnRepo.getRepositoryUrl());

            //customConfig.put( REAL_SVN_LATEST_REVISION, String.valueOf( svnRepo.getLatestRevision() ) );

            svnRepo.setRepositoryUrl(getTagsUrl(plan, tag));
            svnRepo.setReferencesDifferentRepository(false);
        }
    }

    public void untagBuildConfig(CurrentBuildResult results, Repository repo, Map<String, String> customConfig) {
        if (repo instanceof SvnRepository && customConfig != null) {
            SvnRepository svnRepo = (SvnRepository) repo;
            String repoUrl = (String) customConfig.get(REAL_SVN_REPOSITORY_URL);
            if (repoUrl != null) {
                svnRepo.setRepositoryUrl(repoUrl);
                customConfig.remove(REAL_SVN_REPOSITORY_URL);

                String realLatestRevision = (String) customConfig.get(REAL_SVN_LATEST_REVISION);
                log.debug("realLatestRevision = " + realLatestRevision);
                //if ( realLatestRevision != null )
                //svnRepo.setLatestRevision( Long.parseLong( realLatestRevision ) );
            }
        }
    }

    public void appendTagVersion(Plan plan, String tag) {
        SvnRepository repo = (SvnRepository) plan.getBuildDefinition().getRepository();
        String url = repo.getRepositoryUrl();
        if (tag != null) {
            if (!url.endsWith("/")) {
                url += "/";
            }
            url += tag;
        }
        repo.setRepositoryUrl(url);
    }

}
