package net.customware.bamboo.plugin.tagbuild.condition;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.atlassian.bamboo.author.ExtendedAuthorManager;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.bamboo.event.BuildCompletedEvent;
import com.atlassian.bamboo.event.BuildEvent;
import com.atlassian.bamboo.notification.AbstractNotificationType;
import com.atlassian.bamboo.utils.DefaultVelocityEngine;
import com.atlassian.mail.Email;

public abstract class AbstractNotificationCondition extends AbstractNotificationType {

    private static final Logger log = Logger.getLogger(AbstractNotificationCondition.class);
    
    /**
     * The standard build results email template.
     */
    public static final String BUILD_RESULTS_TEMPLATE = "com/atlassian/bamboo/emailer/BuildResultsEmail.vm";

    private ExtendedAuthorManager extendedAuthorManager;
    private AdministrationConfigurationManager administrationConfigurationManager;
    private VelocityEngine velocityEngine;
    
    public AbstractNotificationCondition(AdministrationConfigurationManager administrationConfigurationManager) {
    	this.administrationConfigurationManager = administrationConfigurationManager;
    }

    public String getConfigurationData() {
        return "";
    }

    public String getEmailSubject( BuildEvent event ) {
        BuildCompletedEvent bcEvent = ( BuildCompletedEvent ) event;
        return "email";
        //TODO
        //BuildResultsRenderer buildResultsRenderer = createBuildRenderer( bcEvent );
        //return buildResultsRenderer.getOneLineSummary();
    }

    public Email getTextEmail( BuildEvent event, Email email ) {
        BuildCompletedEvent bcEvent = ( BuildCompletedEvent ) event;
        //TODO
        return email;
//        BuildResultsRenderer buildResultsRenderer = createBuildRenderer( bcEvent );
//
//        Context context = new VelocityContext();
//        context.put( "summary", bcEvent.getBuildResultsSummary() );
//        context.put( "buildResults", bcEvent.getBuildResults() );
//        context.put( "build", bcEvent.getBuild() );
//        context.put( "buildUrl", getAdminConfiguration().getBaseUrl() + buildResultsRenderer.getBuildUrl() );
//        initTextEmailVelocityContext( bcEvent, context );
//
//        try {
//            Template velocityTemplate = getVelocityEngine().getTemplate( getTextEmailVelocityTemplate() );
//            Writer writer = new StringWriter();
//            velocityTemplate.merge( context, writer );
//
//            email.setBody( writer.toString() );
//            email.setSubject( getEmailSubject( event ) );
//            email.setMimeType( "text/plain" );
//            return email;
//        } catch ( Exception e ) {
//            LOG.error( "Could not create email content for notification" + getDescription() );
//            return null;
//        }
    }

//    protected BuildResultsRenderer createBuildRenderer( BuildCompletedEvent bcEvent ) {
//        return new BuildResultsRenderer(bcEvent.getBuild().getName(), bcEvent.getBuildResults(), null);
//    	//return new BuildResultsRenderer( bcEvent.getBuild().getName(), bcEvent.getBuildResults(), null, extendedAuthorManager );
//    }
    
    /**
     * Called before executing the text email velocity template.
     * 
     * @param context the velocity context.
     */
    protected abstract void initTextEmailVelocityContext( BuildCompletedEvent event, Context context );

    /**
     * The path to the velocity template to use to create text email messages.
     * 
     * @return the template path.
     */
    protected abstract String getTextEmailVelocityTemplate();

    public String getIMContent( BuildEvent event ) {
        //TODO
        return "";
//        String message = null;
//        try {
//            BuildCompletedEvent bcEvent = ( BuildCompletedEvent ) event;
//            BuildResultsRenderer buildResultsRenderer = createBuildRenderer( bcEvent );
//            message = buildResultsRenderer.getOneLineSummary() + "\n" + getAdminConfiguration().getBaseUrl()
//                    + buildResultsRenderer.getBuildUrl();
//            return message;
//        } catch ( Exception e ) {
//            return null;
//        }
    }

    private AdministrationConfiguration getAdminConfiguration() {
        return administrationConfigurationManager.getAdministrationConfiguration();
    }


    public String getEditHtml() {
        return "";
    }

    public String getViewHtml() {
        return "";
    }

    public void init( String data ) {
        // no data required for this condition
    }

    public ExtendedAuthorManager getExtendedAuthorManager() {
        return extendedAuthorManager;
    }

    public void setExtendedAuthorManager( ExtendedAuthorManager extendedAuthorManager ) {
        this.extendedAuthorManager = extendedAuthorManager;
    }

    public VelocityEngine getVelocityEngine() {
        if ( velocityEngine == null ) {
            velocityEngine = DefaultVelocityEngine.getVelocityEngine();
        }
        return velocityEngine;
    }

}
