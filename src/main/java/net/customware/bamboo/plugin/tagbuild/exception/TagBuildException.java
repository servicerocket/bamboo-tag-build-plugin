package net.customware.bamboo.plugin.tagbuild.exception;

public class TagBuildException extends Exception {

	private static final long serialVersionUID = 1994402874502482704L;

	public TagBuildException() {
        super();
    }

    public TagBuildException( String message, Throwable throwable ) {
        super( message, throwable );
    }

    public TagBuildException( String message ) {
        super( message );
    }

    public TagBuildException( Throwable throwable ) {
        super( throwable );
    }

}
