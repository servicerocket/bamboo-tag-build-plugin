package net.customware.bamboo.plugin.tagbuild.factory;

import java.util.HashMap;
import java.util.Map;

import net.customware.bamboo.plugin.tagbuild.finder.AbstractTagFinder;
import net.customware.bamboo.plugin.tagbuild.finder.CvsTagFinder;
import net.customware.bamboo.plugin.tagbuild.finder.HgTagFinder;
import net.customware.bamboo.plugin.tagbuild.finder.SvnTagFinder;
import net.customware.bamboo.plugin.tagbuild.finder.TagFinder;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plugins.hg.HgRepository;
import com.atlassian.bamboo.repository.AbstractRepository;
import com.atlassian.bamboo.repository.cvsimpl.CVSRepository;
import com.atlassian.bamboo.repository.svn.SvnRepository;
import com.atlassian.bamboo.v2.build.repository.RepositoryV2;

public final class TagFinderFactory {

    private TagFinderFactory() {
    }

    private static Map<Class<? extends AbstractRepository>, AbstractTagFinder> tagFinders = new HashMap<Class<? extends AbstractRepository>, AbstractTagFinder>();
    
    static {
        tagFinders.put(SvnRepository.class, new SvnTagFinder());
        tagFinders.put(CVSRepository.class, new CvsTagFinder());
        tagFinders.put(HgRepository.class, new HgTagFinder());
    }
    
    public static TagFinder createTagFinder(Plan plan) {
        return createTagFinder(plan.getBuildDefinition().getRepository());
    }

    /**
     * Constructs a new TagFinder appropriate for the specified Repository. If
     * none is available, <code>null</code> is returned.
     * 
     * @param repository The repository.
     * @return The TagFinder.
     */
    public static TagFinder createTagFinder(RepositoryV2 repository) {
        return (TagFinder)tagFinders.get(repository.getClass());
    }
}
