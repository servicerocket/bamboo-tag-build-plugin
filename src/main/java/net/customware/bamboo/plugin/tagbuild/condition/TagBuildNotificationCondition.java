package net.customware.bamboo.plugin.tagbuild.condition;

import org.apache.velocity.context.Context;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationManager;
import com.atlassian.bamboo.event.BuildCompletedEvent;
import com.atlassian.bamboo.event.BuildEvent;
import com.atlassian.bamboo.notification.AbstractNotificationType;
import com.atlassian.event.Event;

public class TagBuildNotificationCondition extends AbstractNotificationType {

    private static final String KEY = "net.customware.bamboo.plugin.tagbuild:tagBuildNotification";

    private static final String DESCRIPTION = "Builds From Tags";
    
    private AdministrationConfigurationManager administrationConfigurationManager;

    public TagBuildNotificationCondition(AdministrationConfigurationManager administrationConfigurationManager) {
    	this.administrationConfigurationManager = administrationConfigurationManager;
    }

    public String getDescription() {
        return DESCRIPTION;
    }

    public String getKey() {
        return KEY;
    }

    @Override
    public boolean isNotificationRequired(Event event) {
        if (event instanceof BuildCompletedEvent) {
            BuildCompletedEvent bcEvent = (BuildCompletedEvent) event;
            return false;
            //TODO
            //return bcEvent.getBuildResultsSummary().getCustomBuildData().get( TagFinder.TAG_KEY ) != null;
        }
        return false;
    }

    protected String getTextEmailVelocityTemplate() {
        return "plugins/tagbuild/BuildResultsEmail.vm";
    }

    protected void initTextEmailVelocityContext(BuildCompletedEvent event, Context context) {
        //TODO
        //context.put( "tag", event.getBuildResultsSummary().getCustomBuildData().get( TagFinder.TAG_KEY ) );
    }

    @NotNull
    public String getEmailSubject(BuildEvent event) {
        BuildCompletedEvent buildCompletedEvent = (BuildCompletedEvent) event;
        return "Tag Build";
        //TODO

        //BuildResultsRenderer buildResultsRenderer = new BuildResultsRenderer(buildCompletedEvent.getBuild().getName(), buildCompletedEvent.getBuildResults(), null);
        //return buildResultsRenderer.getOneLineSummary();
    }

    @NotNull
    public String getIMContent(BuildEvent event) {
        BuildCompletedEvent bcEvent = (BuildCompletedEvent) event;
        return "Tag Build";
        //TODO

        //BuildResultsRenderer buildResultsRenderer = new BuildResultsRenderer(bcEvent.getBuild().getName(), bcEvent.getBuildResults(), null);
        //String message = buildResultsRenderer.getOneLineSummary() + "\n" + getAdminConfiguration().getBaseUrl() + buildResultsRenderer.getBuildUrl();
        //return message;
    }

    private AdministrationConfiguration getAdminConfiguration() {
    	return administrationConfigurationManager.getAdministrationConfiguration();
    }

}
