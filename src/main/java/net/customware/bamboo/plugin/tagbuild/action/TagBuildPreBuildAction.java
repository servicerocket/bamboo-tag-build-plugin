package net.customware.bamboo.plugin.tagbuild.action;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.CustomPreBuildAction;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plugins.hg.HgRepository;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import net.customware.bamboo.plugin.tagbuild.finder.HgTagFinder;
import net.customware.bamboo.plugin.tagbuild.util.HgUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;

public class TagBuildPreBuildAction implements CustomPreBuildAction {

    protected BuildContext buildContext;

    protected BuildLoggerManager buildLoggerManager;

    private PlanManager planManager;

    public ErrorCollection validate(BuildConfiguration buildConfiguration) {
        return null;
    }

    public void init(@NotNull BuildContext buildContext) {
        this.buildContext = buildContext;
    }

    @NotNull
    public BuildContext call() throws InterruptedException, Exception {
        BuildLogger buildLogger = buildLoggerManager.getBuildLogger(buildContext.getPlanResultKey());

        if (!isHgRepository()) {
            buildLogger.addBuildLogEntry("[AN34] - Skipping TagBuildPreBuildAction");
            return buildContext;
        }

        String revision = getRevisionFromContext();
        if (revision != null) {
            try {
                buildLogger.addBuildLogEntry("[AN34] - Changing 'tip' to revision: " + revision);
                HgUtils.updateWorkingDirectoryToRevision(revision, getWorkingDirectory());
            } catch (Exception e) {
                buildLogger.addErrorLogEntry("[AN34] - Failed to update revision: " + revision);
                buildLogger.addErrorLogEntry("[AN34] - Failed because: " + e.getMessage());
            }
        } else {
            HgUtils.workWithTip(getWorkingDirectory());
        }

        return buildContext;
    }

    private boolean isHgRepository() {
        return buildContext.getBuildDefinition().getRepository() instanceof HgRepository;
    }

    private File getWorkingDirectory() throws RepositoryException {
        return buildContext.getBuildDefinition().getRepository().getSourceCodeDirectory(buildContext.getPlanKey());
    }

    private String getRevisionFromContext() {
        BuildContext topLevelBuildContext = buildContext;
        // get the most parentBuildContext because that is where the custom properties are stored
        while (topLevelBuildContext.getParentBuildContext() != null) {
            topLevelBuildContext = topLevelBuildContext.getParentBuildContext();
        }

        Plan plan = planManager.getPlanByKey(topLevelBuildContext.getPlanKey());
        Map<String, String> customConfigs = plan.getBuildDefinition().getCustomConfiguration();

        return customConfigs.get(HgTagFinder.MERCURIAL_TARGET_REVISION);
    }

    public void setPlanManager(PlanManager planManager) {
        this.planManager = planManager;
    }

    public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
        this.buildLoggerManager = buildLoggerManager;
    }
}
