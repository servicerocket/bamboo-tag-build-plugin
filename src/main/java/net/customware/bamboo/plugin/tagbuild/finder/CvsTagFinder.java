package net.customware.bamboo.plugin.tagbuild.finder;

import java.util.Collection;
import java.util.Map;

import net.customware.bamboo.plugin.tagbuild.bean.Tag;
import net.customware.bamboo.plugin.tagbuild.exception.TagFinderException;

import org.apache.log4j.Logger;
import org.netbeans.lib.cvsclient.command.CommandAbortedException;
import org.netbeans.lib.cvsclient.connection.AuthenticationException;
import org.netbeans.lib.cvsclient.connection.Connection;

import com.atlassian.bamboo.build.VariableSubstitutionBean;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.cvsimpl.CVSRepository;
import com.atlassian.bamboo.security.StringEncrypter;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.opensymphony.xwork.TextProvider;

public class CvsTagFinder extends AbstractTagFinder {

    public static final String AUTH_SSH = "ssh";

    public static final String AUTH_PASSWORD = "password";

    private static final String REPO_PREFIX = "repository.cvs.";

    public static final String CVS_ROOT = REPO_PREFIX + "cvsRoot";

    public static final String CVS_PASSWORD = REPO_PREFIX + "password";

    public static final String CVS_QUIET_PERIOD = REPO_PREFIX + "quietPeriod";

    public static final String CVS_MODULE = REPO_PREFIX + "module";

    public static final String CVS_BRANCH = REPO_PREFIX + "branchName";

    public static final String CVS_AUTH_TYPE = REPO_PREFIX + "authType";

    public static final String CVS_KEY_FILE = REPO_PREFIX + "keyFile";

    public static final String CVS_PASSPHRASE = REPO_PREFIX + "passphrase";

    public static final String CVS_LAST_UPDATE = REPO_PREFIX + "lastUpdate";

    private static final String REAL_CVS_BRANCH_NAME = "custom.tagbuild.cvs.realBranchName";

    private static final Logger log = Logger.getLogger(CvsTagFinder.class);

    private final transient StringEncrypter stringEncrypter = new StringEncrypter();

    private VariableSubstitutionBean variableSubstitutionBean;

    private TextProvider textProvider;

    public CvsTagFinder() {
    }

    public Collection<Tag> findTags(Plan plan, BuildConfiguration configuration) throws TagFinderException {
        Repository repo = plan.getBuildDefinition().getRepository();
        if (repo instanceof CVSRepository) {
            CVSRepository cvsRepo = (CVSRepository) repo;

            try {
                Connection connection = openConnectionToCvs(cvsRepo, configuration);
            } catch (CommandAbortedException e) {
                log.error(e,e);
                throw new TagFinderException(e);
            } catch (AuthenticationException e) {
            	log.error(e,e);
                throw new TagFinderException(e);
            }
        }
        return null;
    }

    private Connection openConnectionToCvs(CVSRepository cvsRepo, BuildConfiguration config)
            throws CommandAbortedException, AuthenticationException, TagFinderException {
//        CVSRoot root;
//        String authType = config.getString( CVS_AUTH_TYPE );
//
//        String keyFile;
//        String passphrase;
//        if ( AUTH_SSH.equals( authType ) ) {
//            keyFile = config.getString( CVS_KEY_FILE );
//            passphrase = config.getString( CVS_PASSPHRASE );
//            validateCvsRoot( cvsRepo.getCvsRoot() );
//
//            // Validate that the key file exists
//            String filename = config.getString( CVS_KEY_FILE );
//            File file = new File( filename );
//            if ( !file.exists() ) {
//                throw new TagFinderException( textProvider.getText( "repository.keyFile.error" ) );
//            }
//        } else {
//            String password = config.getString( CVS_PASSWORD );
//            validateCvsRoot( cvsRepo.getCvsRoot() );
//        }
//
//        if ( AUTH_SSH.equals( authType ) ) {
//            root = CVSRoot.parse( getSubstitutedCvsRoot( cvsRepo.getCvsRoot() ) );
//        } else {
//            root = getPasswordConfiguredCvsRoot( getSubstitutedCvsRoot( cvsRepo.getCvsRoot() ), cvsRepo
//                    .getPassword() );
//        }
//
//        String protocol = root.getMethod();
//
//        if ( CVSRoot.METHOD_EXT.equals( protocol ) ) {
//            Connection connection = ConnectionFactory.getConnection( root );
//
//            if ( connection instanceof ExtConnection ) {
//                ExtConnection extConnection = ( ExtConnection ) connection;
//                try {
//                    if ( AUTH_SSH.equals(authType) ) {
//                        extConnection.setAuthType( AUTH_SSH );
//                        extConnection.setKeyFile( keyFile );
//                        extConnection.setPassphrase( stringEncrypter.decrypt( passphrase ) );
//                    } else {
//                        extConnection.setAuthType( AUTH_PASSWORD );
//                    }
//                    extConnection.open();
//                } catch ( AuthenticationException e ) {
//                    closeConnection( extConnection );
//                    throw e;
//                } catch ( CommandAbortedException e ) {
//                    closeConnection( extConnection );
//                    throw e;
//                }
//                return extConnection;
//            } else {
//                return connection;
//            }
//
//        } else {
//            Connection connection = null;
//            try {
//                connection = ConnectionFactory.getConnection( root );
//                connection.open();
//            } catch ( AuthenticationException e ) {
//                closeConnection( connection );
//                throw e;
//            } catch ( CommandAbortedException e ) {
//                closeConnection( connection );
//                throw e;
//            }
//            return connection;
//        }
        return null;
    }

//    private void closeConnection( Connection connection ) {
//        if ( connection != null ) {
//            try {
//                connection.close();
//            } catch ( IOException e ) {
//                LOG.warn( "Failed to close CVS server connection", e );
//            }
//        }
//    }
//
//    private void validateCvsRoot( String cvsPath ) {
//        CVSRoot cvsRoot;
//        try {
//            if ( authType != null && authType.equals( AUTH_SSH ) ) {
//                cvsRoot = CVSRoot.parse( cvsPath );
//            } else {
//                cvsRoot = getPasswordConfiguredCvsRoot( cvsPath, getPassword() );
//            }
//        } catch ( Exception e ) {
//            throw new TagFinderException("Invalid CVS root format:" + e.getMessage(), e );
//        }
//
//        String protocol = cvsRoot.getMethod();
//        if ( !( CVSRoot.METHOD_EXT.equals( protocol ) || CVSRoot.METHOD_PSERVER.equals( protocol )
//                || CVSRoot.METHOD_LOCAL.equals( protocol ) || CVSRoot.METHOD_FORK.equals( protocol ) || cvsRoot
//                .isLocal() ) ) {
//            throw new TagFinderException( "Unsupported cvs protocol: " + protocol );
//        }
//
//        if ( CVSRoot.METHOD_EXT.equals( protocol ) ) {
//
//            ExtConnection extConnection = null;
//            try {
//                Connection connection = ConnectionFactory.getConnection( cvsRoot );
//
//                if ( connection instanceof ExtConnection ) {
//                    extConnection = ( ExtConnection ) connection;
//                    if ( authType.equals( AUTH_SSH ) ) {
//                        extConnection.setAuthType( AUTH_SSH );
//                        extConnection.setKeyFile( keyFile );
//                        extConnection.setPassphrase( stringEncrypter.decrypt( passphrase ) );
//                    } else {
//                        extConnection.setAuthType( AUTH_PASSWORD );
//                    }
//                    extConnection.verify();
//                } else {
//                    connection.verify();
//                }
//            } catch ( Exception e ) {
//                errorCollection.addError( CVS_ROOT, "Cannot connect to CVS root: " + getException( e )
//                        + ". Your server details / password may be incorrect" );
//            } finally {
//                closeConnection( extConnection );
//            }
//        } else {
//            Connection connection = null;
//            try {
//                connection = ConnectionFactory.getConnection( cvsRoot );
//                connection.verify();
//            } catch ( Exception e ) {
//                errorCollection.addError( CVS_ROOT, "Cannot connect to CVS root: " + getException( e ) );
//            } finally {
//                closeConnection( connection );
//            }
//        }
//
//    }
//
//    public String getSubstitutedCvsRoot( String cvsRoot ) {
//        String substitutedRoot = variableSubstitutionBean.substituteBambooVariables( cvsRoot, null, null );
//        return substitutedRoot;
//    }
//
//    private CVSRoot getPasswordConfiguredCvsRoot( String cvsRootString, String password ) {
//        PasswordOverrideCVSRoot cvsRoot = new PasswordOverrideCVSRoot( cvsRootString );
//        cvsRoot.setPassword( password );
//        return cvsRoot;
//    }

    protected void tagBuildConfig(Plan plan, String tag, Repository repo, Map<String,String> customConfig) {
        if (repo instanceof CVSRepository) {
            CVSRepository cvsRepo = (CVSRepository) repo;
            customConfig.put(REAL_CVS_BRANCH_NAME, String.valueOf(cvsRepo.getBranchName()));

            cvsRepo.setBranchName(tag);
        }

    }

    protected void untagBuildConfig(CurrentBuildResult results, Repository repo, Map<String,String> customConfig) {
        if (repo instanceof CVSRepository) {
            String realBranchName = (String) customConfig.get(REAL_CVS_BRANCH_NAME);
            customConfig.remove(REAL_CVS_BRANCH_NAME);

            CVSRepository cvsRepo = (CVSRepository) repo;
            cvsRepo.setBranchName(realBranchName);
        }
    }


    public void appendTagVersion(Plan plan, String tag) {
        CVSRepository repo = (CVSRepository) plan.getBuildDefinition().getRepository();
        String url = repo.getBranchName();
        if (tag != null) {
            if (!url.endsWith("/")) {
                url += "/";
            }
            url += tag;
        }
        repo.setBranchName(url);
    }

    public VariableSubstitutionBean getVariableSubstitutionBean() {
        return variableSubstitutionBean;
    }

    public void setVariableSubstitutionBean(VariableSubstitutionBean variableSubstitutionBean) {
        this.variableSubstitutionBean = variableSubstitutionBean;
    }

    public TextProvider getTextProvider() {
        return textProvider;
    }

    public void setTextProvider(TextProvider textProvider) {
        this.textProvider = textProvider;
    }

}
