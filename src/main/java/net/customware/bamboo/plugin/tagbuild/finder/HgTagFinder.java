package net.customware.bamboo.plugin.tagbuild.finder;

import com.atlassian.bamboo.author.ExtendedAuthor;
import com.atlassian.bamboo.author.ExtendedAuthorManager;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plugins.hg.AuthenticationType;
import com.atlassian.bamboo.plugins.hg.HgRepository;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.resultsummary.BuildResultsSummaryManager;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.utils.process.*;
import net.customware.bamboo.plugin.tagbuild.bean.Tag;
import net.customware.bamboo.plugin.tagbuild.exception.TagFinderException;
import net.customware.bamboo.plugin.tagbuild.mercurial.handler.HgRevisionLogOutputHandler;
import net.customware.bamboo.plugin.tagbuild.mercurial.handler.HgTagsOutputHandler;
import net.customware.bamboo.plugin.tagbuild.mercurial.util.HgRepositoryUrlObfuscator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class HgTagFinder extends AbstractTagFinder {

    public static final String MERCURIAL_TARGET_REVISION = "mercurial.target.revision";

    public static final String TIP = "tip";

    private BuildResultsSummaryManager buildResultsSummaryManager;

    private ExtendedAuthorManager extendedAuthorManager;

    private static final Logger log = Logger.getLogger(SvnTagFinder.class);

    private ExtendedAuthor getAuthor(String authorName) {
        if (authorName != null) {
            ExtendedAuthor author = extendedAuthorManager.getExtendedAuthorByName(authorName);
            log.debug("author = " + author);
            return author;
        }
        return null;
    }

    private void execute(List<String> commandArgs, File workingDirectory, OutputHandler outputHandler) throws TagFinderException {
        workingDirectory.mkdirs();
        if (outputHandler == null) {
            outputHandler = new StringOutputHandler();
        }
        PluggableProcessHandler handler = new PluggableProcessHandler();
        handler.setOutputHandler(outputHandler);
        StringOutputHandler errorHandler = new StringOutputHandler();
        handler.setErrorHandler(errorHandler);
        ExternalProcess process = (new ExternalProcessBuilder()).command(commandArgs, workingDirectory).handler(handler).build();
        process.setTimeout(TimeUnit.SECONDS.toMillis(60));
        process.execute();
        if (!handler.succeeded())
            throw new TagFinderException((new StringBuilder()).append("command ").append(HgRepositoryUrlObfuscator.obfuscatePasswordsInUrls(commandArgs)).append(" failed. Working directory was `").append(workingDirectory).append("'.").toString());
        else
            return;
    }

    public static URI wrapWithUsernameAndPassword(HgRepository repository) {
        URI remoteUri = repository.getRemoteUri();
        AuthenticationType authenticationType = repository.getAuthenticationType();
        if (authenticationType == AuthenticationType.BYPASS)
            return remoteUri;
        String username = repository.getUsername();
        String password = repository.getPassword();

        boolean usePassword = authenticationType == AuthenticationType.PASSWORD && StringUtils.isNotBlank(password);
        String authority = StringUtils.isEmpty(username) ? null : usePassword ? (new StringBuilder()).append(username).append(":").append(password).toString() : username;
        try {
            return new URI(remoteUri.getScheme(), authority, remoteUri.getHost(), remoteUri.getPort(), remoteUri.getPath(), remoteUri.getQuery(), remoteUri.getFragment());
        } catch (URISyntaxException e) {
            log.error("Cannot parse remote URI: " + remoteUri);
            throw new RuntimeException(e);
        }
    }

    private void pullCloneRepository(HgRepository repo, File sourceDirectory) throws TagFinderException {
        sourceDirectory.mkdirs();
        List<String> commands = new ArrayList<String>();
        if (repo.isHgExecutableSet()) {
            commands.add(repo.getHgExeCapability());
        } else {
            commands.add("hg");
        }

        if (sourceDirectory.listFiles().length > 0) {
            // If there are already files, assume that the clone has already been done, thus we only need to do a pull
            commands.add("pull");
        } else {
            // Otherwise, run the clone command
            commands.add("clone");
            URI uri = wrapWithUsernameAndPassword(repo);

            commands.add(uri.toString());
            commands.add("./");
        }

        execute(commands, sourceDirectory, null);
    }

    /* return a list of unique ChangeSetIds */
    protected Set<String> retrieveTags(HgRepository repo, File sourceDirectory) throws TagFinderException {
        sourceDirectory.mkdirs();

        List<String> commands = new ArrayList<String>();
        if (repo.isHgExecutableSet()) {
            commands.add(repo.getHgExeCapability());
        } else {
            commands.add("hg");
        }
        commands.add("tags");

        HgTagsOutputHandler handler = new HgTagsOutputHandler();

        execute(commands, sourceDirectory, handler);

        return handler.getTagsByChangeSetIds();
    }

    protected Tag getTagInfo(Plan plan, String changeSetId, File sourceDirectory) throws TagFinderException {
        sourceDirectory.mkdirs();

        HgRepository repo = (HgRepository) plan.getBuildDefinition().getRepositoryV2();

        List<String> commands = new ArrayList<String>();
        if (repo.isHgExecutableSet()) {
            commands.add(repo.getHgExeCapability());
        } else {
            commands.add("hg");
        }
        commands.add("log");
        commands.add("--rev");
        commands.add(changeSetId);

        HgRevisionLogOutputHandler handler = new HgRevisionLogOutputHandler();

        execute(commands, sourceDirectory, handler);

        ExtendedAuthor author = getAuthor(handler.getString("user"));
        Date date = handler.getDate("date");
        String tagName = handler.getString("tag");

        Tag tag = new Tag(buildResultsSummaryManager, plan, tagName, date, author);
        // we can get the revsion with handler.getString("changeset") if we need to
        tag.setRevision(changeSetId);

        return tag;
    }

    private Collection<Tag> findTags(Plan plan) throws TagFinderException {
        try {
            HgRepository hgRepo = (HgRepository) plan.getBuildDefinition().getRepository();
            File sourceDirectory = hgRepo.getSourceCodeDirectory(plan.getKey());

            pullCloneRepository(hgRepo, sourceDirectory);

            Set<String> tagList = retrieveTags(hgRepo, sourceDirectory);

            List<Tag> tags = new ArrayList<Tag>();

            for (String key : tagList) {

                // Tag tag = getTagInfo(plan, key, tagList.getProperty(key), sourceDirectory);
                Tag tag = getTagInfo(plan, key, sourceDirectory);

                if (!TIP.equals(tag.getName()))
                    tags.add(tag);

            }

            // TODO give this a comparator to sort the tags better
            return tags;
        } catch (Exception e) {
            throw new TagFinderException(e);
        }

    }

    public Collection<Tag> findTags(Plan plan, BuildConfiguration buildConfiguration) throws TagFinderException {
        return findTags(plan);
    }

    @Override
    protected void tagBuildConfig(Plan plan, String tag, Repository repo, Map<String, String> customConfig) {

        Tag targetTag = null;
        try {
            for (Tag aTag : findTags(plan)) {
                //System.out.println("*********************" + aTag.getName() + " = " + tag + " ? " + aTag.getName().equals(tag));
                if (aTag.getName().equals(tag)) {
                    targetTag = aTag;
                    break;
                }
            }
            if (targetTag != null && customConfig != null) {
                customConfig.put(MERCURIAL_TARGET_REVISION, targetTag.getRevision());
            }
        } catch (TagFinderException e) {
            log.error(e, e);
        }
    }

    @Override
    protected void untagBuildConfig(CurrentBuildResult results, Repository repo, Map<String, String> customConfig) {
        if (customConfig != null) {
            customConfig.remove(MERCURIAL_TARGET_REVISION);
        }
    }

    @Override
    protected void updateRepoWorkingDirectory(Repository repo, Map customConfig, Plan plan) {
        // Not needed
    }

    @Override
    public void appendTagVersion(Plan plan, String tag) {
        // Not needed
    }

    public void setExtendedAuthorManager(ExtendedAuthorManager extendedAuthorManager) {
        this.extendedAuthorManager = extendedAuthorManager;
    }

    public void setBuildResultsSummaryManager(BuildResultsSummaryManager buildResultsSummaryManager) {
        this.buildResultsSummaryManager = buildResultsSummaryManager;
    }
}
