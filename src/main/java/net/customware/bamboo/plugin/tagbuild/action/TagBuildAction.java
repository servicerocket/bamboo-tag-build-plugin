package net.customware.bamboo.plugin.tagbuild.action;

import com.atlassian.bamboo.author.ExtendedAuthorManager;
import com.atlassian.bamboo.build.SimpleLogEntry;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.ww2.actions.PlanActionSupport;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.bamboo.ww2.aware.BuildConfigurationAware;
import com.atlassian.bamboo.ww2.aware.permissions.PlanExecuteSecurityAware;
import com.opensymphony.util.TextUtils;
import net.customware.bamboo.plugin.tagbuild.bean.Tag;
import net.customware.bamboo.plugin.tagbuild.exception.TagFinderException;
import net.customware.bamboo.plugin.tagbuild.factory.TagFinderFactory;
import net.customware.bamboo.plugin.tagbuild.finder.TagFinder;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagBuildAction extends PlanActionSupport implements PlanExecuteSecurityAware, BuildConfigurationAware {

    private static final long serialVersionUID = -832483046153381808L;

    private static final Logger log = Logger.getLogger(TagBuildAction.class);

    public static final String MERCURIAL_TAG_FILTER = "custom.customware.tag.filter";

    private ExtendedAuthorManager extendedAuthorManager;

    private Collection<Tag> tags;

    private String tag;

    private BuildConfiguration buildConfiguration;

    private String tagFilter;

    @Override
    public String execute() throws Exception {

        if (tags == null) {
            try {
                TagFinder tagFinder = getTagFinder();
                if (tagFinder != null) {
                    tags = tagFinder.findTags(getPlan(), getBuildConfiguration());
                }
            } catch (TagFinderException e) {
                log.error(e, e);
                addActionError(e.getMessage());
                return ERROR;
            }
        }

        // filter tags by name
        tags = filterTagResults(tags);

        return super.execute();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public String doBuild() throws Exception {
        //Buildable build1 = getBuild();
        if (!getPlan().isExecuting()) {
            getTagFinder().buildTag(getPlan(), tag);

            getTagFinder().appendTagVersion(getPlan(), tag);

            planExecutionManager.startManualExecution(getPlan(), getUser());

            log.info(getPlan().getBuildLogger().addBuildLogEntry(new SimpleLogEntry("Tag build requested: " + tag)));
        } else {
            log.info(getPlan().getBuildLogger().addBuildLogEntry(new SimpleLogEntry("Manual build request received, but build already triggered.")));
        }

        return SUCCESS;
    }

    public String doExecute() throws Exception {

        tagFilter = retrieveFilterRegex();

        return INPUT;
    }

    public String doTagFilter() throws Exception {

        try {
            if( getTagFilter()  == null || getTagFilter().trim().isEmpty() ) {
                storeFilterRegex(null);
            } else {
                // test if it is valid
                Pattern.compile(getTagFilter());
                storeFilterRegex(getTagFilter());
            }
        } catch (Exception e) {
            addActionError("Failed to compile regex: " + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }

    private String retrieveFilterRegex() {
        return getPlan().getBuildDefinition().getCustomConfiguration().get(MERCURIAL_TAG_FILTER);
    }

    private void storeFilterRegex(String regex) {
        getPlan().getBuildDefinition().getCustomConfiguration().put(MERCURIAL_TAG_FILTER, regex);
    }

    private TagFinder getTagFinder() {
        Repository repo = getPlan().getBuildDefinition().getRepository();
        return TagFinderFactory.createTagFinder(repo);
    }

    public Collection<Tag> getTags() {
        return tags;
    }

    public Collection filterTagResults(Collection<Tag> tags) {
        String regex = retrieveFilterRegex();

        if (!TextUtils.stringSet(regex) || regex.startsWith("*")) {
            return tags;
        }

        Collection<Tag> filteredTags = new ArrayList<Tag>();

        Pattern pattern = Pattern.compile(regex);
        for (Tag tag : tags) {
            Matcher matcher = pattern.matcher(tag.getName());
            if (matcher.find())
                filteredTags.add(tag);
        }

        return filteredTags;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isShowOperations() {
        return true;
    }

    public ExtendedAuthorManager getExtendedAuthorManager() {
        return extendedAuthorManager;
    }

    public void setExtendedAuthorManager(ExtendedAuthorManager extendedAuthorManager) {
        this.extendedAuthorManager = extendedAuthorManager;
    }

    public BuildConfiguration getBuildConfiguration() {
        return buildConfiguration;
    }

    public void setBuildConfiguration(BuildConfiguration buildConfiguration) {
        this.buildConfiguration = buildConfiguration;
    }

    public String getTagFilter() {
        return tagFilter;
    }

    public void setTagFilter(String tagFilter) {
        this.tagFilter = tagFilter;
    }
}
