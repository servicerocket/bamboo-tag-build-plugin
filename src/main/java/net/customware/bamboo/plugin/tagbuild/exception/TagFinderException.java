package net.customware.bamboo.plugin.tagbuild.exception;

public class TagFinderException extends Exception {

	private static final long serialVersionUID = -6893327225062942929L;

	public TagFinderException() {
        super();
    }

    public TagFinderException( String message, Throwable cause ) {
        super( message, cause );
    }

    public TagFinderException( String message ) {
        super( message );
    }

    public TagFinderException( Throwable cause ) {
        super( cause );
    }

}
