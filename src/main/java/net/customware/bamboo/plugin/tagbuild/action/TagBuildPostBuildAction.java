package net.customware.bamboo.plugin.tagbuild.action;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.CustomPostBuildCompletedAction;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plugins.hg.HgRepository;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.v2.build.BuildContext;
import net.customware.bamboo.plugin.tagbuild.finder.HgTagFinder;
import net.customware.bamboo.plugin.tagbuild.util.HgUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: aslwyn.wong
 * Date: 22/06/11
 * Time: 1:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class TagBuildPostBuildAction implements CustomPostBuildCompletedAction {

    protected BuildContext buildContext;

    protected BuildLoggerManager buildLoggerManager;

    private PlanManager planManager;

    @Override
    public void init(@NotNull BuildContext buildContext) {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws InterruptedException, Exception {
        final PlanResultKey planResultKey = buildContext.getPlanResultKey();
        if (isHgRepository()) {
            BuildLogger planLogger = buildLoggerManager.getBuildLogger(planResultKey.getPlanKey());

            try {
                planLogger.addBuildLogEntry("[AN34] - Changing 'revision' back to 'tip'");
                HgUtils.workWithTip(getWorkingDirectory());
            } catch (Exception e) {
                planLogger.addErrorLogEntry("[AN34] - Failed to revert revision back to tip");
            } finally {
                // remove
                removeRevisionFromContext();
            }
        }
        return buildContext;
    }

    private boolean isHgRepository() {
        return buildContext.getBuildDefinition().getRepository() instanceof HgRepository;
    }

    private void removeRevisionFromContext() {
        BuildContext topLevelBuildContext = buildContext;
        // get the most parentBuildContext because that is where the custom properties are stored
        while (topLevelBuildContext.getParentBuildContext() != null) {
            topLevelBuildContext = topLevelBuildContext.getParentBuildContext();
        }

        Plan plan = planManager.getPlanByKey(topLevelBuildContext.getPlanKey());
        Map<String, String> customConfigs = plan.getBuildDefinition().getCustomConfiguration();

        if (customConfigs != null) {
            customConfigs.remove(HgTagFinder.MERCURIAL_TARGET_REVISION);
        }

    }

    private File getWorkingDirectory() throws RepositoryException {
        return buildContext.getBuildDefinition().getRepository().getSourceCodeDirectory(buildContext.getPlanKey());
    }

    public void setPlanManager(PlanManager planManager) {
        this.planManager = planManager;
    }

    public void setBuildLoggerManager(BuildLoggerManager buildLoggerManager) {
        this.buildLoggerManager = buildLoggerManager;
    }
}

