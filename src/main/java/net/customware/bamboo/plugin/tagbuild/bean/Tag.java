package net.customware.bamboo.plugin.tagbuild.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import net.customware.bamboo.plugin.tagbuild.finder.TagFinder;

import com.atlassian.bamboo.author.ExtendedAuthor;
import com.atlassian.bamboo.chains.ChainResultsSummary;
import com.atlassian.bamboo.chains.ChainStageResult;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.resultsummary.BuildResultsSummary;
import com.atlassian.bamboo.resultsummary.BuildResultsSummaryManager;
import com.atlassian.bamboo.resultsummary.ResultsSummary;

public class Tag {

    private Plan plan;

    private String name;

    private Date date;

    private ExtendedAuthor author;

    private BuildResultsSummaryManager buildResultsSummaryManager;

    private String revision;

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public Tag(BuildResultsSummaryManager buildResultsSummaryManager, Plan plan, String name) {
        this(buildResultsSummaryManager, plan, name, null, null);
    }

    public Tag(BuildResultsSummaryManager buildResultsSummaryManager, Plan plan, String name, Date date) {
        this(buildResultsSummaryManager, plan, name, date, null);
    }

    public Tag(BuildResultsSummaryManager buildResultsSummaryManager, Plan plan, String name, Date date, ExtendedAuthor author) {
        this.plan = plan;
        this.name = name;
        this.date = date;
        this.author = author;
        this.buildResultsSummaryManager = buildResultsSummaryManager;
    }

    public ExtendedAuthor getAuthor() {
        return author;
    }

    public Date getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    /**
     * Returns the build results for this build which are for this tag. If none
     * are availabe, an empty collection will be returned.
     *
     * @return The collection of build results.
     */
    public Collection<ResultsSummary> getBuildResultSummaries() {
        List<ResultsSummary> tagSummaries = new ArrayList<ResultsSummary>();
        List<ChainResultsSummary> allSummaries = null;
        try {
            allSummaries = buildResultsSummaryManager.getAllResultSummariesForPlan(plan);
        } catch (Exception e) {
            allSummaries = Collections.emptyList();
        }

        for (ChainResultsSummary summary : allSummaries) {
            boolean tagFound = false;
            for(ChainStageResult stages : summary.getStageResults()) {
                for(BuildResultsSummary result :stages.getBuildResults()) {
                    String summaryTag = result.getCustomBuildData().get(TagFinder.TAG_KEY);
                    if (name.equals(summaryTag)) {
                        tagSummaries.add(summary);
                        tagFound = true;
                        break;
                    }
                }
                if(tagFound) {
                    break;
                }
            }
        }

        Collections.reverse(tagSummaries);
        return tagSummaries;
    }
}
