package net.customware.bamboo.plugin.tagbuild.mercurial.handler;

import com.atlassian.utils.process.LineOutputHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HgRevisionLogOutputHandler extends LineOutputHandler {

    private static SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy Z");

    private Map<String, String> logDetails = new HashMap<String, String>();

    public Date getDate(String key) {
        try {
            return sdf.parse(getString(key));
        } catch (ParseException e) {
            return null;
        }
    }

    public String getString(String key){
        return logDetails.get(key);
    }

    @Override
    protected void processLine(int i, String s) {
        int split = s.indexOf(":");
        if (split > 0 && split+1 <s.length()) {
            String key = s.substring(0, split).trim();
            String value = s.substring(split + 1).trim();
            logDetails.put(key, value);
        }
    }
}
