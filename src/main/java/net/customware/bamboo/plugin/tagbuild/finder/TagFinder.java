package net.customware.bamboo.plugin.tagbuild.finder;

import java.util.Collection;

import net.customware.bamboo.plugin.tagbuild.bean.Tag;
import net.customware.bamboo.plugin.tagbuild.exception.TagFinderException;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;

public interface TagFinder {

    /**
     * The key used to store the tag value in the build results summary custom
     * data.
     */
    public static final String TAG_KEY = "custom.tagbuild.tag";

    /**
     * The key used to store the working directory value in the build results
     * summary custom data.
     */
    public static final String WORKING_DIRECTORY_KEY = "custom.tagbuild.workingDirectory";

    /**
     * Returns a list of Strings containing the available tags for the current
     * repository.
     * 
     * @param plan The build.
     * @param buildConfiguration The build configuration.
     * 
     * @return the collection of tags for the current build.
     */
    public Collection<Tag> findTags(Plan plan, BuildConfiguration buildConfiguration) throws TagFinderException;

    /**
     * Sets the build up to build a tagged version rather than the latest
     * version.
     * 
     * @param plan The build to set to 'tag' mode.
     * @param tag The tag to build.
     */
    public void buildTag(Plan plan, String tag);

    /**
     * Appends the tag version to the end of the repository url.
     * 
     * @param plan The build to set to 'tag' mode.
     * @param tag The tag to build.
     */
    public abstract void appendTagVersion( Plan plan, String tag );

    /**
     * Reverts a previous tag of the build.
     * 
     * @param plan The plan to reset.
     * @param results The build results for the most recent build.
     */
    public void buildReset(Plan plan, CurrentBuildResult results);
}
