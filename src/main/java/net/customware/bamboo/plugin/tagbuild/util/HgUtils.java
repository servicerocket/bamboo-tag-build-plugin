package net.customware.bamboo.plugin.tagbuild.util;

import com.atlassian.utils.process.*;
import net.customware.bamboo.plugin.tagbuild.mercurial.util.HgRepositoryUrlObfuscator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA.
 * User: aslwyn.wong
 * Date: 23/06/11
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class HgUtils {

    public static final String TIP_REVISION = "tip";

    public static void execute(List<String> commandArgs, File workingDirectory) throws Exception {
        OutputHandler outputHandler = new StringOutputHandler();
        PluggableProcessHandler handler = new PluggableProcessHandler();
        handler.setOutputHandler(outputHandler);
        StringOutputHandler errorHandler = new StringOutputHandler();
        handler.setErrorHandler(errorHandler);
        ExternalProcess process = (new ExternalProcessBuilder()).command(commandArgs, workingDirectory).handler(handler).build();
        process.setTimeout(TimeUnit.SECONDS.toMillis(60));
        process.execute();

        if (!handler.succeeded())
            throw new Exception((new StringBuilder()).append("command ").append(HgRepositoryUrlObfuscator.obfuscatePasswordsInUrls(commandArgs)).append(" failed. Working directory was `").append(workingDirectory).append("'.").toString());
    }

    public static void updateWorkingDirectoryToRevision(String revision, File workingDirectory) throws Exception {
        List commandArgs = new ArrayList();
        commandArgs.add("hg");
        commandArgs.add("update");
        commandArgs.add("--rev");
        commandArgs.add(revision);

        HgUtils.execute(commandArgs, workingDirectory);
    }

    public static void workWithTip(File workingDirectory) throws Exception {
        updateWorkingDirectoryToRevision(TIP_REVISION, workingDirectory);
    }
}
