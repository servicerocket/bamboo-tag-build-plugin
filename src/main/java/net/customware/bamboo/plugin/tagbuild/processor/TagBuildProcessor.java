package net.customware.bamboo.plugin.tagbuild.processor;


import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.v2.build.task.AbstractBuildTask;
import net.customware.bamboo.plugin.tagbuild.factory.TagFinderFactory;
import net.customware.bamboo.plugin.tagbuild.finder.TagFinder;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.v2.build.BuildContext;

public class TagBuildProcessor extends AbstractBuildTask implements CustomBuildProcessor {


	private PlanManager planManager;

    public TagBuildProcessor() {
    }

    public BuildContext call() throws Exception {
        BuildContext topLevelBuildContext = buildContext;
        while(topLevelBuildContext.getParentBuildContext() != null) {
            topLevelBuildContext = topLevelBuildContext.getParentBuildContext();
        }

        Plan plan = planManager.getPlanByKey(topLevelBuildContext.getPlanKey());
        TagFinder tagFinder = TagFinderFactory.createTagFinder(plan);

        tagFinder.buildReset(plan, buildContext.getBuildResult());

        return buildContext;
	}

    public PlanManager getPlanManager() {
        return planManager;
    }

    public void setPlanManager(PlanManager planManager) {

        this.planManager = planManager;
    }
}
