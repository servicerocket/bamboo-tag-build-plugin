package net.customware.bamboo.plugin.tagbuild.mercurial.handler;

import com.atlassian.utils.process.OutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 * It is easier to get the unique changesetid for all the tags oppose to revision numbers
 */
public class HgTagsOutputHandler implements OutputHandler {

    private Set<String> rawTagLines = new HashSet();

    public Set<String> getTagsByChangeSetIds() {
        return rawLinesToTags();
    }

    /* Currently assumes that the changeSetId is always after the first colon. In reality it is probably after the last one.
    * TODO improve with regex
    * */
    private Set rawLinesToTags() {
        Set tags = new HashSet();
        if (rawTagLines != null) {
            for (String tagsAsString : rawTagLines) {
                if (tagsAsString.contains(":")) {
                    String[] tokenized = tagsAsString.split(":");
                    // changeset key and name/rev value
                    tags.add(tokenized[1]);
                }
            }
        }

        return tags;
    }

    public void process(InputStream inputStream) throws ProcessException {

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            while ((line = br.readLine()) != null) {
                rawTagLines.add(line);
            }

            br.close();
            // tags.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void complete() {
    }

    public void setWatchdog(Watchdog watchdog) {
    }
}
