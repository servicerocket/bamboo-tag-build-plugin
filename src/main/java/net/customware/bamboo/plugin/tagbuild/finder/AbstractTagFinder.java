package net.customware.bamboo.plugin.tagbuild.finder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.atlassian.bamboo.build.SimpleLogEntry;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.repository.AbstractRepository;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.atlassian.spring.container.ContainerManager;

public abstract class AbstractTagFinder implements TagFinder {

    private static final Logger log = Logger.getLogger(AbstractTagFinder.class);

    private PlanManager planManager;

    public void setPlanManager(PlanManager planManager) {
        this.planManager = planManager;
    }

    public PlanManager getPlanManager() {
        return planManager;
    }
    
    public AbstractTagFinder() {
        ContainerManager.autowireComponent(this);
    }

    public void buildTag(Plan plan, String tag) {

        if (tag == null)
            return;

        Repository repo = plan.getBuildDefinition().getRepository();
        Map<String,String> customConfig = plan.getBuildDefinition().getCustomConfiguration();
        if (customConfig == null) {
            customConfig = new HashMap<String, String>();
            plan.getBuildDefinition().setCustomConfiguration(customConfig);
        }

        customConfig.put(TAG_KEY, tag);

        // HG tag finder will override this guy (not needed)
        updateRepoWorkingDirectory(repo, customConfig, plan);

        tagBuildConfig(plan, tag, repo, customConfig);
    }

    protected void updateRepoWorkingDirectory(Repository repo, Map customConfig, Plan plan) {
        if (repo instanceof AbstractRepository) {
            AbstractRepository aRepo = (AbstractRepository) repo;

            File workingDirectory = aRepo.getWorkingDirectory();
            if (workingDirectory != null) {
                customConfig.put(WORKING_DIRECTORY_KEY, workingDirectory.getPath());

                workingDirectory = getTagbuildWorkingDirectory(workingDirectory);
                // Remove any old build directory which may exist for this
                // build.
                cleanSourceDirectory(plan, workingDirectory);

                aRepo.setWorkingDir(workingDirectory);
            }
        }
    }

    public abstract void appendTagVersion(Plan plan, String tag);

    /**
     * This method is called after the initial standard configuration of the
     * repository. The working directory for the repository will have already
     * been redirected to the temporary location. Implementors should add any
     * repository-specific additions here.
     *
     * @param plan         The build.
     * @param tag          The tag to build.
     * @param repo         The repository.
     * @param customConfig The custom configuration map.
     */
    protected abstract void tagBuildConfig(Plan plan, String tag, Repository repo, Map<String,String> customConfig);

    private File getTagbuildWorkingDirectory(File workingDirectory) {
        return new File(workingDirectory.getParentFile(), "tagbuild-dir");
    }

    private void cleanSourceDirectory(Plan plan, File workingDirectory) {
        File sourceDirectory = new File(workingDirectory, plan.getKey());
        if (sourceDirectory.exists()) {

            try {
                FileUtils.forceDelete(sourceDirectory);
            } catch (IOException e) {
                log.error(plan.getBuildLogger().addBuildLogEntry(new SimpleLogEntry(
                        "Unable to delete tagbuild source directory: " + sourceDirectory)), e);
            }
        }
    }

    /**
     * Resets any customisations from building the tag.
     */
    public void buildReset(Plan plan, CurrentBuildResult results) {
        Repository repo = plan.getBuildDefinition().getRepository();
        Map<String,String> customConfig = plan.getBuildDefinition().getCustomConfiguration();

        if (customConfig != null) {
            String tag = (String) customConfig.get(TAG_KEY);

            if (StringUtils.isNotEmpty(tag)) {
                try {
	                customConfig.remove(TAG_KEY);
	                results.getCustomBuildData().put(TAG_KEY, tag);
	
	                if (repo instanceof AbstractRepository) {
	                    AbstractRepository aRepo = (AbstractRepository) repo;
	
	                    // Were we able to reroute the working directory?
	                    String workingPath = (String) customConfig.get(WORKING_DIRECTORY_KEY);
	                    if (workingPath != null) {
	                        // Reset the working directory
	                        File workingDirectory = new File(workingPath);
	                        aRepo.setWorkingDir(workingDirectory);
	                        aRepo.setReferencesDifferentRepository(false);
	                    } else {
	                        // We have to refresh the directory.
	                        aRepo.setReferencesDifferentRepository(true);
	                    }
	
	                }
                } catch(Exception e) {
                    log.error(e,e);
                }
            }
        }

        untagBuildConfig(results, repo, customConfig);
    }

    /**
     * This is called after the build to reset any customisations for the tagged build.
     *
     * @param results      The results.
     * @param repo         The repository.
     * @param customConfig The custom configuration map.
     */
    protected abstract void untagBuildConfig(CurrentBuildResult results, Repository repo, Map<String,String> customConfig);

}
