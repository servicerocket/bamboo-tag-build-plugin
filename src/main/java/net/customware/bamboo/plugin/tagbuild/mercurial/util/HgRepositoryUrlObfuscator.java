package net.customware.bamboo.plugin.tagbuild.mercurial.util;

import java.util.ArrayList;
import java.util.List;

public class HgRepositoryUrlObfuscator {

    public HgRepositoryUrlObfuscator()
    {
    }

    public static String obfuscatePasswordInUrl(String url)
    {
        int startIndex = url.indexOf("://");
        if(startIndex != -1)
        {
            startIndex += 3;
            String userdata = url.substring(startIndex).trim();
            int endIndex = userdata.indexOf('@');
            if(endIndex != -1)
            {
                endIndex += startIndex;
                startIndex = url.indexOf(':', startIndex);
                if(startIndex != -1 && startIndex < endIndex)
                    url = (new StringBuilder()).append(url.substring(0, startIndex + 1)).append("***").append(url.substring(endIndex)).toString();
            }
        }
        return url;
    }

    public static List<String> obfuscatePasswordsInUrls(List<String> urls)
    {
        List<String> filteredUrls = new ArrayList<String>(urls.size());
        for (String url : urls) {
        	filteredUrls.add(obfuscatePasswordInUrl(url));
		}
        return filteredUrls;
    }
}
