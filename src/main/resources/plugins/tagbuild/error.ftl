[#-- @ftlvariable name="action" type="com.atlassian.bamboo.logger.AdminErrorAction" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.logger.AdminErrorAction" --]

<html>
<head>
	<title>[@ww.text name='error.title' /] rwarubf</title>

    <meta name="decorator" content="plan"/>
    <meta name="tab" content="tagBuild"/>
</head>

<body>
	<h1>[@ww.text name='error.heading' /]</h1>

    [#if formattedErrorMessages?has_content]
        [@ui.messageBox type='error']
            [#list formattedErrorMessages as error]
                <p>${error}</p>
            [/#list]
        [/@ui.messageBox]
    [/#if]

    <a class="internalLink" href="${req.contextPath}/build/tagbuild/tagbuild.action?buildKey=${buildKey}">Show Tags</a>

</body>
</html>