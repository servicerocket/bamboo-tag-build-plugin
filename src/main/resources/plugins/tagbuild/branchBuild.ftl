<html>
<head>
    <title>${build.name}: Branches</title>
</head>

<body>

[@ui.header page='Branch Build' object='${build.name}' /]
[@cp.buildSubMenu selectedTab='branchBuild' /]

<div class="section">
    <p class="marginned">
        This page lists the branches available for <strong>${build.name}</strong>, as well as any builds run for the branch.
    </p>

	[#if tags?exists && tags?size gt 0]
        [#list tags as tag]
        
        	<h2>${tag.name}</h2>
        	
        	<p>
	        	Created <b>${tag.date?date}</b> by <b><a href="[@cp.displayAuthorOrProfileLink author=tag.author /]">${tag.author.fullName}</a></b>.
        	</p>
        
        	<p>
        	<a class="internalLink" href="${req.contextPath}/plugins/tagbuild/branchbuild!build.action?buildKey=${build.key}&tag=${tag.name}"><img src="${req.contextPath}/images/jt/icn_run.gif" border="0" hspace="2" alt="Checkout & Build" title="Checkout & Build"/></a>
        	<a class="internalLink" href="${req.contextPath}/plugins/tagbuild/branchbuild!build.action?buildKey=${build.key}&tag=${tag.name}">Checkout & Build</a></p>
        
			[#assign resultsList = tag.buildResultSummaries]
			
			[#if (pager.page.list)?has_content]
			    [#assign resultsList = pager.page.list]
			[/#if]
			
		    [#if resultsList?has_content]
			    <table class="grid">
			        <tr>
			            <th>Build Number</th>
			            <th>Reason</th>
			            <th>Date</th>
			            <th>Duration</th>
			            <th>Test Results</th>
			            [#if showOperations]
			                <th>Operations</th>
			            [/#if]
			        </tr>
			        [#list resultsList as buildResult]
			            <tr [#if buildResult_index % 2 == 1] class="altRow" [/#if]>
			                <td>
			                    [#if buildResult.buildKey?exists]
			                        <a id="buildResult:${buildResult.buildKey}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${buildResult.buildKey}-${buildResult.buildNumber}">${buildResult.buildKey}-${buildResult.buildNumber}</a>
			                    [#else]
			                        <a id="buildResult:${build.key}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${build.key}-${buildResult.buildNumber}">Build ${buildResult.buildNumber}</a>
			                    [/#if]
			                    [@cp.commentIndicator buildResultsSummary=buildResult /]
			                </td>
			                <td>
			                    [#if buildResult.reasonForBuild?exists && buildResult.reasonForBuild.reason == "Code has changed"]
			                        [#if buildResult.buildKey?exists]
			                            <a id="commits:${buildResult.buildKey}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${buildResult.buildKey}-${buildResult.buildNumber}/commit" >${buildResult.reasonSummary}</a>
			                            [#if buildResult.changesListSummary?exists]
			                                [@dj.tooltip target="commits:${buildResult.buildKey}-${buildResult.buildNumber}" text=buildResult.changesListSummary /]
			                            [/#if]
			                        [#else]
			                            <a id="commits:${build.key}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${build.key}-${buildResult.buildNumber}/commit" >${buildResult.reasonSummary}</a>
			                            [#if buildResult.changesListSummary?exists]
			                                [@dj.tooltip target="commits:${build.key}-${buildResult.buildNumber}" text=buildResult.changesListSummary /]
			                            [/#if]
			                        [/#if]
			                    [#else]
			                        <div id="${buildResult.buildState}">${buildResult.reasonSummary}</div>
			                    [/#if]
			                    
			                </td>
			                <td><div id="${buildResult.buildState}">${buildResult.relativeBuildDate}</div></td>
			                <td><div id="${buildResult.buildState}">${buildResult.durationDescription}</div></td>
			                <td><div id="${buildResult.buildState}">
			                    [#if (build?exists && build.buildDefinition.builder.hasTests()) || buildResult.testSummary?exists]
			                        ${buildResult.testSummary}
			                    [#else]
			                        Testless build
			                    [/#if]
			                </div></td>
			                [#if showOperations]
			                <td>
			                    [#if fn.hasPerm('/build/admin/deleteBuildResultsBuilds.action')]
			                    <a id="deleteBuildResult:${build.key}-${buildResult.buildNumber}" href="${req.contextPath}/plugins/tagbuild/deleteBuildResultsBuilds.action?buildNumber=${buildResult.buildNumber}&buildKey=${build.key}" title="Delete the build results" onclick="return confirm('Are you sure you want to delete this build?');"> <img src="${req.contextPath}/images/icons/trash_16.gif" width="16" height="16" border="0" align="absmiddle"></a>
			                    [/#if]
			                </td>
			                [/#if]
			            </tr>
			        [/#list]
			        [#if (pager.page)?has_content]
			            <tr>
			                <td colspan="[#if showOperations]6[#else]5[/#if]">
			                    [@cp.pagination /]
			                </td>
			            </tr>
			        [/#if]        
			    </table>
		    [/#if]

        [/#list]

	[#else]
	    <p class="marginned">
	        There are no branches available for ${build.name}.
	    </p>
	[/#if]

</div>

</body>
</html>
