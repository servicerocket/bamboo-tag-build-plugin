<html>
<head>
    <title>${plan.name}: Tags</title>
     <meta name="decorator" content="plan"/>
     <meta name="tab" content="tagBuild"/>
</head>

<body>

<div class="section">

    <div class="aui-message info">

        [#if tagFilter?exists]Tags are filtered with the following regex: <strong>${tagFilter}</strong>[/#if]

        <form action="${req.contextPath}/build/tagbuild/tagFilter.action" class="aui">
            <label for="tagFilter">Regex Filter</label>
            <input class="text" type="text" name="tagFilter" value="[#if tagFilter?exists]${tagFilter}[/#if]" />
            <input type="hidden" name="buildKey" value="${buildKey}">
            <input class="button" type="submit" value="update">
        </form>
        <span class="aui-icon icon-info"></span>
    </div>

    <p class="marginned">
        This page lists the tags available for <strong>${plan.name}</strong>, as well as any builds already run for the tag.
    </p>

	[#if tags?exists && tags?size gt 0]
        [#list tags as tag]

        	<h2>${tag.name}</h2>

        	<p>
	        	Created <b>${tag.date?date}</b>
	        	[#if tag.author?exists]
	        	by <b><a href="[@cp.displayAuthorOrProfileLink author=tag.author /]">${tag.author.name}</a></b>.
	        	[/#if]
        	</p>

        	<p>
        	<a class="internalLink" href="${req.contextPath}/build/tagbuild/tagbuild!build.action?buildKey=${buildKey}&tag=${tag.name}"><img src="${req.contextPath}/images/jt/icn_run.gif" border="0" hspace="2" alt="Checkout & Build" title="Checkout & Build"/></a>
        	<a class="internalLink" href="${req.contextPath}/build/tagbuild/tagbuild!build.action?buildKey=${buildKey}&tag=${tag.name}">Checkout & Build</a></p>

			[#assign resultsList = tag.buildResultSummaries]

			[#if (pager.page.list)?has_content]
			    [#assign resultsList = pager.page.list]
			[/#if]

		    [#if resultsList?has_content]
			    <table class="aui">
			        <thead>
			        <tr>
			            <th>Build Number</th>
			            <th>Reason</th>
			            <th>Date</th>
			            <th>Duration</th>
			            <th>Test Results</th>
			            [#if showOperations]
			                <th>Operations</th>
			            [/#if]
			        </tr>
			        </thead>
			        [#list resultsList as buildResult]
			            <tr [#if buildResult_index % 2 == 1] class="altRow" [/#if]>
			                <td>
			                    [#if buildResult.buildKey?exists]
			                        <a id="buildResult:${buildResult.buildKey}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${buildResult.buildKey}-${buildResult.buildNumber}">${buildResult.buildKey}-${buildResult.buildNumber}</a>
			                    [#else]
			                        <a id="buildResult:${buildKey}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${buildKey}-${buildResult.buildNumber}">Build ${buildResult.buildNumber}</a>
			                    [/#if]
			                    [@cp.commentIndicator buildResultsSummary=buildResult /]
			                </td>
			                <td>
			                    [#if buildResult.reasonForBuild?exists && buildResult.reasonForBuild.reason == "Code has changed"]
			                        [#if buildResult.buildKey?exists]
			                            <a id="commits:${buildResult.buildKey}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${buildResult.buildKey}-${buildResult.buildNumber}/commit" >${buildResult.reasonSummary}</a>
			                            [#if buildResult.changesListSummary?exists]
			                                [@dj.tooltip target="commits:${buildResult.buildKey}-${buildResult.buildNumber}" text=buildResult.changesListSummary /]
			                            [/#if]
			                        [#else]
			                            <a id="commits:${buildKey}-${buildResult.buildNumber}" class="${buildResult.buildState}" href="${req.contextPath}/browse/${buildKey}-${buildResult.buildNumber}/commit" >${buildResult.reasonSummary}</a>
			                            [#if buildResult.changesListSummary?exists]
			                                [@dj.tooltip target="commits:${buildKey}-${buildResult.buildNumber}" text=buildResult.changesListSummary /]
			                            [/#if]
			                        [/#if]
			                    [#else]
			                        <div id="${buildResult.buildState}">${buildResult.reasonSummary}</div>
			                    [/#if]

			                </td>
			                <td><div id="${buildResult.buildState}">${buildResult.relativeBuildDate}</div></td>
			                <td><div id="${buildResult.buildState}">${buildResult.durationDescription}</div></td>
			                <td><div id="${buildResult.buildState}">
			                    [#if (build?exists && build.buildDefinition.builder.hasTests()) || buildResult.testSummary?exists]
			                        ${buildResult.testSummary}
			                    [#else]
			                        Testless build
			                    [/#if]
			                </div></td>
			                [#if showOperations]
			                <td>
			                    [#if fn.hasPlanPermission('WRITE',plan)]
                                    [@ww.url id='deleteUrl' namespace='/build/admin' action='deletePlanResults' buildNumber='${buildResult.buildNumber}' buildKey='${plan.key}'/]
                                        <a class="deleteLink" id="deleteBuildResult_${buildResult.plan.key}-${buildResult.buildNumber}"
                                           href="${deleteUrl}"
                                           title="Delete the build results"
                                           onclick="return confirm('[@ww.text name="buildResult.completedBuilds.confirmDelete"/]');">[@ww.text name='global.buttons.delete'/]
                                        </a>

			                    [/#if]
			                </td>
			                [/#if]
			            </tr>
			        [/#list]
			        [#if (pager.page)?has_content]
			            <tr>
			                <td colspan="[#if showOperations]6[#else]5[/#if]">
			                    [@cp.pagination /]
			                </td>
			            </tr>
			        [/#if]
			    </table>
		    [/#if]

        [/#list]

	[#else]
	    <p class="marginned">
	        There are no tags available for ${plan.name}.
	    </p>
	[/#if]

</div>

</body>
</html>
